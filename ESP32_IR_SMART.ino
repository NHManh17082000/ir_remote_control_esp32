#include <Arduino_JSON.h>
#include "BluetoothSerial.h"
#include <WiFi.h>
#include <String.h>
#include <Preferences.h>  //using Preferences library to store wifiname and pass to flash. link : https://randomnerdtutorials.com/esp32-save-data-permanently-preferences/
#include <nvs_flash.h>
#include <HardwareSerial.h>
const char* Wifiname ;
const char* password ;

//
BluetoothSerial SerialBT;
Preferences preferences;
//Tạo một web server tại cổng 80 - cổng mặc định cho esp32 webserver
//create a webserver in port 80 - defauld port of esp32 webserver
WiFiServer webServer(80);
//uint8_t flag=0;
char str[100];
int i=0;
unsigned long timeout =0;   // timeout of connecting wifi 
unsigned long timeout1 =0;   // timeout of connecting wifi 


void setup() {
  Serial1.begin(115200, SERIAL_8N1, 16, 17);
  preferences.begin("my_space", false);
  String flash_wifiname = preferences.getString("Wifiname", ""); 
  String flash_password = preferences.getString("password", "");
  //Wifiname = flash_wifiname.c_str();
  //password = flash_password.c_str();
  //// Serial1.println(Wifiname);
  if (flash_wifiname == "" || flash_password == ""){
    // Serial1.println("No values saved for ssid or password");
    initBT();
  }
  else
  {
    WiFi.begin(flash_wifiname.c_str(), flash_password.c_str());
    // Serial1.print("Connecting to WiFi ..");
    timeout1=millis();
    while (WiFi.status() != WL_CONNECTED) 
    {
      // Serial1.print('.');
      delay(1000);
      if(millis()-timeout1 >= 10000)
      {
        // Serial1.println("error to connect to wifi");
        preferences.clear();
        //preferences.end();
        initBT();
        break;
      }
    }
    // Serial1.println(WiFi.localIP());
    webServer.begin();
    //Serial1.begin(115200);
  }
  //initBT();
  
}

void initBT(){
  
  if(!SerialBT.begin("IR smart life 3")){
    // Serial1.println("An error occurred initializing Bluetooth");
    ESP.restart();
  }else{
    // Serial1.println("Bluetooth initialized");
  }

  SerialBT.register_callback(btCallback);
  // Serial1.println("The device started, now you can pair it with bluetooth");
}

void btCallback(esp_spp_cb_event_t event, esp_spp_cb_param_t *param){
  if(event == ESP_SPP_SRV_OPEN_EVT){
    // Serial1.println("Client Connected!");
  }
  else if(event == ESP_SPP_DATA_IND_EVT){
    // Serial1.printf("ESP_SPP_DATA_IND_EVT len=%d, handle=%d\n\n", param->data_ind.len, param->data_ind.handle);
        String received = bluetoothReadLine();
        // Serial1.println(received);
        char* str = &received[0];
        Wifiname = strtok(str,",");
        // Serial1.println(Wifiname);
        while(Wifiname != NULL) {
        //Chỉ dịnh đối số NULL trong hàm strtok để tiếp tục tách chuỗi ban đầu
        password = strtok(NULL, ",");
        // Serial1.println(password);
        WiFi.begin(Wifiname, password);
        timeout = millis();
        //// Serial1.println(timeout);
        // Serial1.print("Connecting...");
        while (WiFi.status() != WL_CONNECTED) 
        {
            delay(500);
            // Serial1.println("connecting...");
            // Serial1.println(millis());
            if(millis()-timeout >= 10000)
            {
              // Serial1.println("out1");
              break;
            }
        }
        // Serial1.println("out2");
        if (WiFi.status() != WL_CONNECTED)
        {
           writeSerialBT("0");
           // Serial1.print("ERROR");
           delay(2000);
           break;
        }
        else
        {
          preferences.putString("Wifiname", Wifiname); 
          preferences.putString("password", password);
          preferences.end();
          // Serial1.print("Local IP: ");
          // Serial1.print(WiFi.localIP());
          // Serial1.print("/");
          String ip = ip2Str(WiFi.localIP())+"/";
          delay(2000);
          writeSerialBT(ip);
          //WiFiServer webServer(80);
          webServer.begin();
          //Serial1.begin(115200);
          //flag =1;
          break;
        }
        }
    }
}

String bluetoothReadLine(){
    String text_received = "";
    while(SerialBT.available())
    {
        byte r = SerialBT.read();
        if(r!=13 && r!=10 && char(r)!='\0')
            text_received = text_received + char(r);
    }
    return text_received;      
}
void writeSerialBT(String respuesta){
  SerialBT.println(respuesta);
  SerialBT.flush();
}
String ip2Str(IPAddress ip){
  String s="";
  for (int i=0; i<4; i++) {
    s += i  ? "." + String(ip[i]) : String(ip[i]);
  }
  return s;
}

void loop()
{
   WiFiClient webClient = webServer.available();
   if(webClient)
  {
    // Serial1.println("New web Client");
    //biến lưu giá trị response
    String currentLine = "";
    //nếu có client connect và không quá thời gian time out
    while(webClient.connected())
    {
  //đọc giá trị timer tại thời điểm hiện tại
  //nếu client còn kết nối
      if(webClient.available())
      {
        //đọc giá trị truyền từ client theo từng byte kiểu char
        char c = webClient.read();
        // Serial1.write(c);
        //str[i]= c; // lưu giá trị vào Header
        //// Serial1.write(str[i]);
        //i++;
        if(c == '\n') //Nếu đọc được kí tự xuống dòng (hết chuỗi truyền tới)
        {
          if (currentLine.length() == 0) 
          {
            //esp32 webserver response
            // HTTP headers luôn luôn bắt đầu với code HTTP (ví d HTTP/1.1 200 OK)
            webClient.println("HTTP/1.1 200 OK");
            webClient.println("Content-type:text/html"); // sau đó là kiểu nội dụng mà client gửi tới, ví dụ này là html
            webClient.println("Connection: close"); // kiểu kết nối ở đây là close. Nghĩa là không giữ kết nối sau khi nhận bản tin
            webClient.println();
            //// Serial1.println(str);
            i=0;
            break;
          }
          else
          {
            if (i==0)
            {
              // Serial1.println(currentLine);
              char* buf = &currentLine[0];
              char *token = strtok(buf,"/"); // GET/
              char *token1 = strtok(NULL,"/");  // IR_type
              char *token2 = strtok(NULL," ");  //btn_name
              // Serial1.println(token1);
              // Serial1.println(token2);
              if(strstr(token1,"remove")==NULL)
              {
                JSONVar JSONbuffer;
                JSONbuffer["type"]=token1;
                JSONbuffer["btn"]=token2;
                Serial1.println (JSONbuffer);
              }
              else
              {
                // Serial1.println("remove");
                //preferences.remove(Wifiname);
                //preferences.remove(password);
                //preferences.clear();
                nvs_flash_erase(); // erase the NVS partition and...
                nvs_flash_init(); // initialize the NVS partition.
                //while(true);
                delay(1000);
                ESP.restart();
              }
                     
            }
            currentLine = "";
            i++;
          }
        }
        else if (c != '\r')   //nếu giá trị gửi tới khác xuống duòng
        {
          currentLine += c;     //lưu giá trị vào biến
        }
      }
    }
    
    webClient.stop();
    //// Serial1.println("Client disconnected.");
    //// Serial1.println("");
  }
}
